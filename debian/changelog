libpam-alreadyloggedin (0.3-9) unstable; urgency=medium

  * QA upload.
  * Make autopkgtest python3 compatible. (Closes: #958224)
  * Install library to multiarch directory.
    Thanks to Martin Ziegler for the hint. (Closes: #986247)
  * d/rules:
    - Convert to dh sequencer (Closes: #969122)
    - Enable all hardening options
  * d/control:
    - Bump Standards-Version to 4.5.1
    - Use debhelper compat level 13
    - Change priority from extra to optional
    - Declare that d/rules does not require root
    - Drop unnecessary XS-Testsuite field
    - Add Vcs-* URLs
  * Replace deprecated ADTTMP with AUTOPKGTEST_TMP.
  * Drop d/source/options which included custom compression format.
  * d/copyright: use https URL and correct short name for BSD-3-Clause.
  * Bump d/watch format to version 4.

 -- Reiner Herrmann <reiner@reiner-h.de>  Sun, 25 Apr 2021 10:50:26 +0200

libpam-alreadyloggedin (0.3-8) unstable; urgency=medium

  * QA upload.
  * Fix tests for Python3.

 -- Matthias Klose <doko@debian.org>  Tue, 10 Mar 2020 16:49:11 +0100

libpam-alreadyloggedin (0.3-7) unstable; urgency=medium

  * QA upload.
  * Properly orphan the package (RFA filed in Nov 2014, orphaned via control
    message in May 2016). See #770373.
  * Use python3 in the autopkg test.
  * Bump debhelper and standards versions.

 -- Matthias Klose <doko@debian.org>  Tue, 10 Mar 2020 10:43:58 +0100

libpam-alreadyloggedin (0.3-6) unstable; urgency=low

  * Add “XS-Testsuite: autopkgtest” to debian/control.
  * Improve security warning for the DEP-8 test.

 -- Jakub Wilk <jwilk@debian.org>  Mon, 24 Feb 2014 22:58:28 +0100

libpam-alreadyloggedin (0.3-5) unstable; urgency=low

  * Improve the package description.
  * Improve debian/rules:
    + Don't use dh_testdir; instead use target dependencies to ensure that
      debian/rules is run from the correct directory.
    + Don't assume that build(-arch) was run before binary(-arch).
  * Use dh-buildinfo:
    + Update debian/rules.
    + Add the package to Build-Depends.
  * Update patch descriptions.
  * Add patch (manpage-ref-font.diff) to use bold font for manpage references,
    as recommended by man-pages(7).
  * Force gzip compression for .debian.tar.
  * Compile with -D_FILE_OFFSET_BITS=64.
  * Compile without -DBUG_STAT_MISSING.
  * Update debian/copyright:
    + Update format URI.
    + Bump year range.
    + Fix formatting of the License field.
  * Bump standards version to 3.9.5 (no changes needed).
  * Add DEP-8 tests.

 -- Jakub Wilk <jwilk@debian.org>  Sat, 22 Feb 2014 22:18:35 +0100

libpam-alreadyloggedin (0.3-4) unstable; urgency=low

  * Export CFLAGS and CPPFLAGS flags in debian/rules.
  * Patch upstream makefile to respect *FLAGS from environment.
  * Bumps standards version to 3.9.2 (no changes needed).
  * Use versioned format URI for the copyright file.
  * Rewrite debian/rules without using dh.
    + Reduce minimum required debhelper version to 7.
  * Remove Vcs-* fields.

 -- Jakub Wilk <jwilk@debian.org>  Wed, 28 Sep 2011 18:08:45 +0200

libpam-alreadyloggedin (0.3-3) unstable; urgency=low

  * Bump standards version to 3.9.1 (no changes needed).
  * Add Vcs-* fields.
  * Update my e-mail address.
  * Update debian/copyright to the latest DEP-5 version.
  * Update years in debian/copyright.
  * Revamp debian/rules.
    + Bump build-dependency on debhelper to (>= 7.0.50) for overrides support.
  * Pass CFLAGS and LDFLAGS (get from dpkg-buildflags) to the makefile.
    + Build depend on dpkg-dev (>= 1.15.7).
  * Refresh patches.
  * Extract upstream changelog from the RPM spec file.

 -- Jakub Wilk <jwilk@debian.org>  Fri, 04 Feb 2011 22:33:05 +0100

libpam-alreadyloggedin (0.3-2) unstable; urgency=low

  * Switch to source format 3.0 (quilt).
  * Bump standards version to 3.8.3, no changes needed.
  * Convert debian/copyright to the DEP-5 format.
  * Don't use MAXPATHLEN constant, as it's not available on Hurd.
  * Add security notes to package description and README.Debian
    (closes: #561448). Thanks to Frank Lin PIAT for raising his concerns.
  * Link using gcc rather than ld.

 -- Jakub Wilk <ubanus@users.sf.net>  Tue, 26 Jan 2010 17:29:43 +0100

libpam-alreadyloggedin (0.3-1) unstable; urgency=low

  * Initial release (Closes: #520108).
  * Due to #524608, recommend login (>= 1:4.1.3.1-1).

 -- Jakub Wilk <ubanus@users.sf.net>  Tue, 12 May 2009 10:19:43 +0200
